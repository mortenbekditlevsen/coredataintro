//
//  ViewController.m
//  CoreDataIntro1
//
//  Created by Morten Ditlevsen on 01/10/14.
//  Copyright (c) 2014 Mojo Apps. All rights reserved.
//

#import "ViewController.h"
#import "Service.h"
#import "School.h"
#import "Student.h"

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UILabel *schoolTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *studentsLabel;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear: animated];
    [Service initializeDataModelForManagedObjectContext: self.managedObjectContext];
    
    [self updateLabels];
}

-(void) updateLabels {
    School *school = [Service getSchoolForManagedObjectContext: self.managedObjectContext];
    self.schoolTitleLabel.text = school.name;
    
    NSString *studentsString = @"";
    for (Student *student in school.students) {
        studentsString = [studentsString stringByAppendingFormat: @"%@ %@\n", student.firstName, student.lastName];
    }
    self.studentsLabel.text = studentsString;    
}


@end
