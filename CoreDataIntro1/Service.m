//
//  Service.m
//  CoreDataIntro1
//
//  Created by Morten Ditlevsen on 01/10/14.
//  Copyright (c) 2014 Mojo Apps. All rights reserved.
//

#import "Service.h"
#import "School.h"
#import "Student.h"

@implementation Service

+(void) initializeDataModelForManagedObjectContext: (NSManagedObjectContext*) context {
    
    if ([self getSchoolForManagedObjectContext: context] != nil) {
        return;
    }
    
    School *eaa = [NSEntityDescription insertNewObjectForEntityForName: @"School" inManagedObjectContext:context];
    eaa.name = @"Erhvervsakademi Aarhus";
    eaa.foundedYear = 1999;
    
    for (NSDictionary *studentData in @[@{@"firstName": @"Johanne", @"lastName": @"Bek Ditlevsen"},
                                        @{@"firstName": @"Anders", @"lastName": @"Pedersen"},
                                        @{@"firstName": @"Peter", @"lastName": @"Holm"},
                                        @{@"firstName": @"Christian", @"lastName": @"Sigersted"},
                                        @{@"firstName": @"Søren", @"lastName": @"Brock"},
                                        @{@"firstName": @"Kasper", @"lastName": @"Gulddal"},
                                        @{@"firstName": @"Randi", @"lastName": @"Bierbaum"},
                                        @{@"firstName": @"Morten", @"lastName": @"Due Olsen"},
                                        @{@"firstName": @"Holger", @"lastName": @"Kyhl Nielsen"},
                                        @{@"firstName": @"Steffen", @"lastName": @"Eskildsen"}]) {
        
        Student *student = [NSEntityDescription insertNewObjectForEntityForName: @"Student" inManagedObjectContext:context];
        student.firstName = studentData[@"firstName"];
        student.lastName = studentData[@"lastName"];
        student.school = eaa;
    }
    
    NSError *error;
    [context save: &error];
}


+(School*) getSchoolForManagedObjectContext: (NSManagedObjectContext*) context {
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName: @"School"];
    NSArray *results = [context executeFetchRequest: request error: nil];
    return [results firstObject];
}


@end
