//
//  Service.h
//  CoreDataIntro1
//
//  Created by Morten Ditlevsen on 01/10/14.
//  Copyright (c) 2014 Mojo Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class School;

@interface Service : NSObject
+(void) initializeDataModelForManagedObjectContext: (NSManagedObjectContext*) context;
+(School*) getSchoolForManagedObjectContext: (NSManagedObjectContext*) context;
@end
