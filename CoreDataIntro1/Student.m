//
//  Student.m
//  CoreDataIntro1
//
//  Created by Morten Ditlevsen on 01/10/14.
//  Copyright (c) 2014 Mojo Apps. All rights reserved.
//

#import "Student.h"


@implementation Student

@dynamic firstName;
@dynamic lastName;
@dynamic school;

@end
