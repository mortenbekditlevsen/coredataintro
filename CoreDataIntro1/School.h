//
//  School.h
//  CoreDataIntro1
//
//  Created by Morten Ditlevsen on 01/10/14.
//  Copyright (c) 2014 Mojo Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Student;

@interface School : NSManagedObject

@property (nonatomic) int16_t foundedYear;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *students;
@end

@interface School (CoreDataGeneratedAccessors)

- (void)addStudentsObject:(Student *)value;
- (void)removeStudentsObject:(Student *)value;
- (void)addStudents:(NSSet *)values;
- (void)removeStudents:(NSSet *)values;

@end
