//
//  School.m
//  CoreDataIntro1
//
//  Created by Morten Ditlevsen on 01/10/14.
//  Copyright (c) 2014 Mojo Apps. All rights reserved.
//

#import "School.h"
#import "Student.h"


@implementation School

@dynamic foundedYear;
@dynamic name;
@dynamic students;

@end
